<?php

namespace app\validators;

use yii\base\DynamicModel;
use yii\validators\Validator;

/**
 * Валидатор вложеных массивов
 * Class ArrayValidator
 * @package app\validators
 */
class ArrayValidator extends Validator
{
    /**
     * Список правил
     * @var array
     */
    public $rules = [];
    /**
     * @inheritdoc
     * @param \yii\base\Model $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute)
    {
        if (!is_array($model->$attribute)) {
            $this->addError($model, $attribute, 'Attribute has been array');
        }

        $form = DynamicModel::validateData($model->$attribute, $this->rules);
        $model->$attribute = $form->getAttributes();

        foreach($form->getErrors() as $error) {
            $this->addError($model, $attribute, $error);
        }
    }
}