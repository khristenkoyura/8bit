Тестовое задание
============================

# Разворачиваем приложение

```sh
$ git clone git@bitbucket.org:khristenkoyura/8bit.git 8bit
$ cd 8bit
$ composer update
$ ./yii client/locations
```

В файле config/console.php [components.client.names] можно раскомментировать другие locations для тестирования