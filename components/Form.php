<?php

namespace app\components;

use yii\base\InvalidParamException;
use yii\base\Model;

class Form extends Model
{

    /**
     * Загрузка и валидациия данных
     * @param $data
     */
    public function loadAndValidate($data)
    {
        $this->load($data);
        if (!$this->validate()) {
            $errors = $this->getFirstErrors();
            list($attribute, $message) = each($errors);
            throw new InvalidParamException("$attribute error: $message");
        }
    }
}