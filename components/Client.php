<?php


namespace app\components;

use app\models\ClientForm;
use app\models\Location;
use app\models\LocationsForm;
use yii\base\Component;
use yii\base\InvalidParamException;
use yii\di\Instance;
use yii\helpers\Json;
use yii\web\BadRequestHttpException;

class Client extends Component
{
    /**
     * @var \MCurl\Client|string|array
     */
    public $curl;

    /**
     * основной адрес
     * @var string
     */
    public $url;

    /**
     * Мап имён запросов к пути url
     * @var string[]
     */
    public $names;

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->curl = Instance::ensure($this->curl);
    }

    /**
     * Клиентский запрос
     * @param $name
     * @return mixed
     * @throws \Exception
     */
    public function getRequest($name)
    {
        $link = $this->url . (isset($this->names[$name]) ? $this->names[$name] : $name) . '.json';

        $result = $this->curl->get($link);
        \Yii::info("curl request: $link");

        if ($result->hasError()) {
            throw new BadRequestHttpException('Client request error: ' . $result->getError(), $result->getErrorCode());
        }

        $json = Json::decode($result->body, true);

        $form = new ClientForm();
        $form->loadAndValidate($json);

        if (!$form->success) {
            $message = $form->data['message'];
            $code = $form->data['code'];
            throw new InvalidParamException("Client bad success: $message", $code);
        }

        return $form->data;
    }

    /**
     * Список гео точек
     * @return Location[]
     * @throws \Exception
     */
    public function getLocations()
    {
        $data = $this->getRequest('locations');
        $form = new LocationsForm();

        $form->loadAndValidate($data);

        return $form->locations;
    }
}