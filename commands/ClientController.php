<?php

namespace app\commands;

use yii\console\Controller;


class ClientController extends Controller
{

    public function actionLocations()
    {
        $client = \Yii::$app->client;
        $locations = $client->getLocations();
        print_r($locations);
    }
}