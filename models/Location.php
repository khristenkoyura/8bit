<?php

namespace app\models;

use yii\base\Model;

class Location extends Model
{
    /**
     * Название месности
     * @var
     */
    public $name;

    /**
     * @var double
     */
    public $latitude;

    /**
     * @var double
     */
    public $longitude;

    /**
     * @inheritdoc
     * @return string
     */
    public function formName()
    {
        return '';
    }
}