<?php

namespace app\models;

use app\components\Form;
use app\validators\ArrayValidator;

class ClientForm extends Form
{
    /**
     * Результат запроса
     * @var mixed
     */
    public $data;

    /**
     * Флаг успешно отработаного запроса
     * @var boolean
     */
    public $success;

    /**
     * @inheritdoc
     * @return array
     */
    public function rules()
    {
        return [
            [['data', 'success'], 'required'],
            ['success', 'boolean'],
            /**
             * Валидация ошибочного результата
             */
            ['data', ArrayValidator::className(), 'rules' => [
                [['message', 'code'], 'required'],
                ['message', 'string', 'min' => 1, 'max' => 1024],
                ['code', 'integer', 'min' => 0],
            ], 'when' => function($model){
                return !$model->success;
            }]
        ];
    }

    /**
     * @inheritdoc
     * @return string
     */
    public function formName()
    {
        return '';
    }
}