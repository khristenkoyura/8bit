<?php

namespace app\models;

use app\components\Form;
use app\validators\ArrayValidator;

class LocationsForm extends Form
{
    /**
     * Список
     * @var array
     */
    public $locations;

    public function rules()
    {
        return [
            ['locations', 'required'],
            ['locations', 'each', 'rule' => [
                ArrayValidator::className(), 'rules' => [
                    ['name', 'required'],
                    ['name', 'string', 'min' => 1, 'max' => 64],
                    ['coordinates', ArrayValidator::className(), 'rules' => [
                        [['lat', 'long'], 'required'],
                        ['lat', 'double', 'min' => -90, 'max' => 90],
                        ['long', 'double', 'min' => -180, 'max' => 180],
                    ]],
                ],
            ]],
            ['locations', 'each', 'rule' => [
                'filter', 'filter' => function($location){
                    $model = new Location();
                    $model->setAttributes([
                        'name' => $location['name'],
                        'latitude' => $location['coordinates']['lat'],
                        'longitude' => $location['coordinates']['long'],
                    ], false);
                    return $model;
                }
            ]],
        ];
    }

    /**
     * @inheritdoc
     * @return string
     */
    public function formName()
    {
        return '';
    }
}