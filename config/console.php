<?php

$config = [
    'id' => 'basic-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'app\commands',
    'components' => [
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'curl' => [
            'class' => '\\MCurl\\Client',
        ],
        'client' => [
            'class' => 'app\\components\\Client',
            'curl' => 'curl',
            'url' => 'https://gist.githubusercontent.com/anonymous/',
            'names' => [
                // Success
                'locations' => 'f44d8e31ba11a6901e6fb51697f6f771/raw/24d807e9a70cef2059f649bc7289e11f7243f94f/5719fb6de4b01190df607ab5',

                // Раскомментируем locations для проверки валидации
                // Error response (https://gist.github.com/anonymous/b4b5aa52b0d3b025efa20043c32ec362)
                //'locations' => 'b4b5aa52b0d3b025efa20043c32ec362/raw/1669411e0e4fe347736b7ec04ae3fcfa6834bfa6/5719fb6de4b01190df607ab5',


            ]
        ],
    ],
    'params' => [],
];

return $config;
